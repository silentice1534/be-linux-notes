## 啟用 drone cicd

1. 到 gitlab 設定頁面，點選 application，申請一個 OAuth

- name: OAuth 名字
- callback url: [http(s)://<domain/>IP>(:<PORT>)]/login，e.g. https://example/login

2. 設定完成後，會生成一個 Client ID 和一個 Client Secret

3. 寫一個 docker-compose.yml 檔案(如 repo 範例檔案)，裡面包涵 drone-server 和 drone-agent

4. 寫一個 .env 檔案，裡面的變數對應的是 docker-compose.yml 內的東西

docker-compose.yml 中的變數

- DRONE_GITLAB_SERVER=[gitlab 的 domain] ，預設 https://gitlab.com
- port: [伺服器對外的 port]:80 ，可以用 nginx 另外設定代理

5. 把這兩個檔案扔到要架 drone 的伺服器上，下`docker-compose up`指令

.env 檔案詳細如下

- DRONE_SERVER_HOST= [<domain/IP>(:<PORT>)]
- DRONE_SERVER_PROTO= [http 或 https]
- DRONE_RPC_SECRET= 用 `openssl rand -hex 16` 生成，server 和 agent 溝通用的 token
- DRONE_GITLAB_CLIENT_ID= (gitlab 生成)
- DRONE_GITLAB_CLIENT_SECRET= (gitlab 生成)

## .drone.yml

1. 安裝 drone-cli

參考 https://docs.drone.io/cli/install/

2. 參考 .drone.yml 檔案

一般變數可以直接在 drone 後台設定

3. 要從 drone-cli 設定 secret 必須先登入，進入 drone setting 頁面

4. **要設定 ssh 必須用 drone-cli**

`drone secret add --repository [<repo>] --name [<name>] --data [<value>/@<path>]`

- repo: e.g. silentice1534/drone-test
- name: secret 名字
- data: 純 value 或檔案
  檔案用@開頭+本幾絕對路徑， e.g. @/Users/tsai_ray/.ssh/id_rsa.pub

## 研究中遇到的坑!

1. gitlab 中的 callback url 不要變來變去，drone 會掛掉收不到 webhook

2. 網路上.drone.yml 中有用到 secret 變數的都是 1.0 以前的版本，基本上都不能用，使用參考 repo drone.yml

3. ssh 密碼一定要用 drone-cli 創建，用 drone 後台頁面建立跑 ci 時會報錯

---

詳細 drone.yml 還沒完善
