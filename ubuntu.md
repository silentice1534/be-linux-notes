# ubuntu  筆記

**安裝所有項目之前，先更新套件**
`sudo apt-get update`

安裝中可能會遇到[Y/n]，是說明安裝的 app 會使用到 xx 空間的硬體，選擇 Y 即可

## -安裝 nginx

安裝指令: `sudo apt-get install nginx`

確認安裝: `nginx -v`

## -安裝 git

安裝指令: `sudo apt-get install git`

確認安裝: `git --version`

## -安裝最新穩定版 node

安裝指令: `sudo apt-get install curl`, `curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -`,`sudo apt install nodejs`

確認安裝: `node -v`

安裝完 node 一併確認 npm(node 的套件管理工具)的安裝

確認 npm 安裝: `npm -v`

## -安裝 npm

安裝指令: `sudo apt install npm`

確認安裝: `npm -v`

安裝完後，用 npm 安裝 yarn，跟 npm 是一樣的工具，但速度比較快

## -安裝 yarn

安裝指令: `sudo npm install yarn -g`

確認安裝: `yarn -v`

## -安裝 docker

安裝指令: `sudo apt-get install docker.io`

確認安裝: `docker -v`

## -安裝 docker-compose

安裝指令: `sudo curl -L "https://github.com/docker/compose/releases/download/1.10.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

給一定的權限: `sudo chmod +x /usr/local/bin/docker-compose`

確認安裝: `docker-compose -v`

### 要 run docker-compose 如果報下面錯誤

**ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?**

在終端機輸入

1. `sudo gpasswd -a ${USER} docker`
2. `sudo su`
3. `su ubuntu`

參考網址:
https://oranwind.org/-solution-qi-dong-docker-compose-fa-sheng-error-couldnt-connect-to-docker-daemon-at-httpdockerlocalunixsocket-is-it-running-cuo-wu/
