# 安裝 gitlab 在 linux(ubuntu)上

**使用的 linux 最好有 2RAM 以上**
硬體要求:https://docs.gitlab.com/ce/install/requirements.html#hardware-requirements

**安裝所有項目之前，先更新套件**
`sudo apt-get update`

## 安裝 curl, SSH 和

git 需要用到 SSH，安裝`openssh-server`

gitlab 支援寄信通知，要安裝`postfix`

安裝指令`sudo apt-get install curl openssh-server ca-certificates postfix`

## 安裝 postfix

安裝指令`sudo apt-get install postfix`

安裝 postfix 時會跳出有兩個網路選項

第一個彈窗選 `Internet Site`

第二個彈窗不用打任何東西管他，直接案`Ok`

## 下載 gitlab server 套件

下載指令`curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash`

## 安裝 gitlab server

安裝指令 `sudo apt-get install gitlab-ce`

## 配置 gitlab 環境

配置指令 `sudo gitlab-ctl reconfigure`

## 修改 gitlab 配置的 port

配置指令 `sudo nano /etc/gitlab/gitlab.rb` (會進到一個編輯畫面 ... 還沒搞懂怎麼操作這個編輯器 ...)
找到**external_url**將網址改為 http://[serverIP]:[port]

**_不要輸入 80 port，會報錯_**

#### 打開 linux port

記得打開 linux 對應的 port

## 重新配置環境

重新配置環境指令:`sudo gitlab-ctl reconfigure`

---

## 在瀏覽器上輸入剛剛修改的 external_url

第一次會要求輸入密碼，之後登入時使用者預設為 root，密碼是剛剛輸入的密碼

## 大功告成

然後就是熟悉的畫面拉！
