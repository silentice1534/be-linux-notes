## 可直接取用模板

## .drone.yml 檔案

1. 安裝 drone-cli

參考 https://docs.drone.io/cli/install/

2. 參考 .drone.yml 檔案

一般變數可以直接在 drone 後台設定

3. 要從 drone-cli 設定 secret 必須先登入，進入 drone setting 頁面

4. **要設定 ssh 必須用 drone-cli**

`drone secret add --repository [<repo>] --name [<name>] --data [<value>/@<path>]`

- repo: e.g. silentice1534/drone-test
- name: secret 名字
- data: 純 value 或檔案
  檔案用@開頭+本幾絕對路徑， e.g. @/Users/tsai_ray/.ssh/id_rsa.pub

### 研究中遇到的坑!

1. gitlab 中的 callback url 不要變來變去，drone 會掛掉收不到 webhook

2. 網路上.drone.yml 中有用到 secret 變數的都是 1.0 以前的版本，基本上都不能用，使用參考 repo drone.yml

3. ssh 密碼一定要用 drone-cli 創建，用 drone 後台頁面建立跑 ci 時會報錯

---

詳細 drone.yml 還沒完善
