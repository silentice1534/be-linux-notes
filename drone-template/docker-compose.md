# 啟用 drone cicd

新增兩個檔案

1. .docker-compose.yml

2. .env

## .docker-compose.yml

1. 到 gitlab 設定頁面，點選 application，申請一個 OAuth

- name: OAuth 名字
- callback url: [http(s)://<domain/>IP>(:<PORT>)]/login，e.g. https://example/login

2. 設定完成後，會生成一個 Client ID 和一個 Client Secret

3. 寫一個 docker-compose.yml 檔案(如 repo 範例檔案)，裡面包涵 drone-server 和 drone-agent

4. 寫一個 .env 檔案，裡面的變數對應的是 docker-compose.yml 內的東西

docker-compose.yml 中的變數

- DRONE_GITLAB_SERVER=[gitlab 的 domain] ，預設 https://gitlab.com
- port: [伺服器對外的 port]:80 ，可以用 nginx 另外設定代理

5. 把這兩個檔案扔到要架 drone 的伺服器上，下`docker-compose up`指令

.env 檔案詳細如下

- DRONE_SERVER_HOST= [<domain/IP>(:<PORT>)]
- DRONE_SERVER_PROTO= [http 或 https]
- DRONE_RPC_SECRET= 用 `openssl rand -hex 16` 生成，server 和 agent 溝通用的 token
- DRONE_GITLAB_CLIENT_ID= (gitlab 生成)
- DRONE_GITLAB_CLIENT_SECRET= (gitlab 生成)

## .env 檔案

### DRONE_SERVER_HOST

自己伺服器域名，不用 `http://`或`https://`
f06d5ddb.ngrok.io

### DRONE_SERVER_PROTO

http 或 https

DRONE_SERVER_PROTO=https

### DRONE_RPC_SECRET

伺服器之間溝通的 token

DRONE_RPC_SECRET=c2ec7d72201bcbe984ff93dee9bcd0

可以下 `openssl rand -hex 16` 產生

### Gitlab 中 application 生一個

DRONE_GITLAB_CLIENT_ID=5c927f52bc5d4232bc0b7f8de9ade16902852d8871c25fb23e3e67b4c578200a
DRONE_GITLAB_CLIENT_SECRET=bfcf93b091b66706364781cbc13cde724f8c78b49250697f58b66c17710bebb1
